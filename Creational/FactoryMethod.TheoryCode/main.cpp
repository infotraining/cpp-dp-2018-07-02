#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <functional>

#include "factory.hpp"

using namespace std;

namespace Canonical
{

class Service
{    
    std::shared_ptr<LoggerCreator> creator_;
public:
    Service(shared_ptr<LoggerCreator> creator) : creator_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        unique_ptr<Logger> logger = creator_->create_logger();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }
protected:
    virtual void run() {}
};

using LoggerFactory = std::unordered_map<std::string, shared_ptr<LoggerCreator>> ;

}

namespace Explain
{
    int add(int a, int b)
    {
        return a + b;
    }

    struct Adder
    {
        int operator()(int a, int b)
        {
            return a + b;
        }
    };

    void function_pointer()
    {
        int (*calc)(int, int) = add;
        cout << calc(4, 5) << endl;
    }

    void functor()
    {
        Adder calc;
        cout << calc(4, 5) << endl;
    }

    void lambda()
    {
        auto calc = [](int a, int b) { return a + b; };
        cout << calc(4, 5) << endl;
    }

    void std_function()
    {
        function<int(int, int)> calc;

        calc = add;
        calc(4, 5);

        calc = Adder{};
        calc(4, 5);

        calc = [](int a, int b) { return a * b; };
        calc(4, 5);
    }
}

namespace Modern
{
    using LoggerCreator = std::function<std::unique_ptr<Logger>()>;

    class Service
    {    
        LoggerCreator creator_;
    public:
        Service(LoggerCreator creator) : creator_(creator)
        {
        }

        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = creator_(); // calling creator
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }
    protected:
        virtual void run() {}
    };

    using LoggerFactory = std::unordered_map<std::string, LoggerCreator>;
}


string load_config_for_logger()
{
    return "DbLogger";
}

int main()
{
    using namespace Modern;

    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger", [] { return std::make_unique<ConsoleLogger>(); }));
    logger_factory.insert(make_pair("FileLogger", [] { return make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", &make_unique<DbLogger>));

    const string logger_id = load_config_for_logger();

    Service srv(logger_factory.at(logger_id));
    srv.use();
}
