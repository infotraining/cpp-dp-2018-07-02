#include "singleton.hpp"
#include <thread>
#include <iostream>

using namespace std;

int main()
{
    std::thread thd{ [] { Singleton::instance().do_something(); } };

    Singleton& singleObject = Singleton::instance();
    singleObject.do_something();

    thd.join();
}
