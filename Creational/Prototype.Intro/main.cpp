#include <cassert>
#include <iostream>
#include <memory>
#include <typeinfo>
#include <cassert>

using namespace std;

class Engine
{
protected:
    virtual std::unique_ptr<Engine> do_clone() const = 0;

public:
    virtual void start() = 0;
    virtual void stop() = 0;
    
    std::unique_ptr<Engine> clone() const
    {
        auto cloned_engine = do_clone();
        assert(typeid(*this) == typeid(*cloned_engine));

        return cloned_engine;
    }
    
    virtual ~Engine() = default;
};

// CRTP
template <typename EngineType>
class CloneableEngine : public Engine
{
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::make_unique<EngineType>(static_cast<const EngineType&>(*this));
    }
};

class Diesel : public Engine
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::make_unique<Diesel>(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::make_unique<TDI>(*this);
    } 
};

class Hybrid : public Engine
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }
protected:
    std::unique_ptr<Engine> do_clone() const override
    {
        return std::make_unique<Hybrid>(*this);
    }
};

class Electric : public CloneableEngine<Electric>
{
public:
    virtual void start() override
    {
        cout << "Electric starts\n";
    }

    virtual void stop() override
    {
        cout << "Electric stops\n";
    }
};

class Car
{
    unique_ptr<Engine> engine_;

public:
    Car(unique_ptr<Engine> engine) : engine_{ move(engine) }
    {
    }

    Car(const Car& source) : engine_{source.engine_->clone()}
    {
    }

    Car& operator=(const Car& source)
    {
        if (this != &source)
        {
            engine_ = source.engine_->clone();
        }

        return *this;
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1{ make_unique<TDI>() };
    c1.drive(100);

    cout << "\n";

    Car c2 = c1;\
    c2.drive(300);
}
