#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

struct StatResult
{
    std::string description;
    double value;

    StatResult(const std::string& desc, double val) : description(desc), value(val)
    {
    }
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

enum StatisticsType
{
    avg,
    min_max,
    sum
};

class Statistics
{
public:
    virtual Results calculate(const Data& data) = 0;
    virtual ~Statistics() = default;
};

class Avg : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        return { {"AVG", avg } };
    }
};

class MinMax : public Statistics
{
public:
    Results calculate(const Data& data) override
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));        

        return { { "MIN" , min }, { "MAX", max } };
    }

};

class Sum : public Statistics
{
public:
    Results calculate(const Data& data) override
    {    
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        return { { "SUM", sum } };
    }
};

// Example of Composite Pattern
class StatGroup : public Statistics
{
    std::vector<Statistics*> stats_;
public:
    Results calculate(const Data& data) override
    {
        Results result_group;

        for(Statistics* s : stats_)
        {
            auto result = s->calculate(data);
            result_group.insert(result_group.end(), result.begin(), result.end());
        }        

        return result_group;
    }

    void add(Statistics* s)
    {
        stats_.push_back(s);
    }
};

class DataAnalyzer
{
    Statistics* stat_;
    Data data_;
    Results results_;

public:
    DataAnalyzer(Statistics* stat) : stat_{stat}
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();
        results_.clear();

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data_.push_back(d);
        }

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void set_statistics(Statistics* stat)
    {
        stat_ = stat;
    }

    void calculate()
    {
        auto result = stat_->calculate(data_);

        results_.insert(results_.end(), result.begin(), result.end());

    }

    const Results& results() const
    {
        return results_;
    }
};

void show_results(const Results& results)
{
    for (const auto& rslt : results)
        std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    Avg avg;
    MinMax min_max;
    Sum sum;

    StatGroup std_stats;
    std_stats.add(&avg);
    std_stats.add(&min_max);
    std_stats.add(&sum);

    DataAnalyzer da{&std_stats};
    da.load_data("data.dat");
    da.calculate();    

    show_results(da.results());

    std::cout << "\n\n";

    da.load_data("new_data.dat");
    da.calculate();

    show_results(da.results());
}