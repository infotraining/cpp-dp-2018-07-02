#include "starbugs_coffee.hpp"
#include <memory>

void client(std::shared_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

int main()
{
    using namespace std;

    auto coffee = 
        make_shared<WhippedCream>(
            make_shared<Whisky>(
                make_shared<ExtraEspresso>(
                    make_shared<ExtraEspresso>(
                        make_shared<Espresso>()))));

    client(coffee);
}
