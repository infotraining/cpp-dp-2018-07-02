#include "proxy.hpp"
#include <vector>
#include <cassert>

using namespace std;

class Client
{
    unique_ptr<Subject> subject_;
public:
    Client(unique_ptr<Subject> subject) : subject_{move(subject)}
	{
	}

	void use()
	{
		subject_->request();
	}
};

int main()
{
    unique_ptr<Proxy> proxy = make_unique<Proxy>("Data");

    Client c {move(proxy)};

	std::cout << std::endl;

	c.use();

	std::cout << std::endl;

	c.use();
}

namespace Cpp98
{
	Proxy* get_proxy1()
	{
		return new Proxy("1");
	}

	Proxy* get_proxy2()
	{
		static Proxy p{"2"};
		return &p;
	}

	void use1(Proxy* ptr)
	{
		if (ptr)
			ptr->request();
	}

	void use2(Proxy* ptr)
	{
		if (ptr)
			ptr->request();
		
		delete ptr;
	}
}

void poiners_cpp98()
{
	using namespace Cpp98;

	// raw pointer
	Proxy* p1 = new Proxy("data1");

	use1(new Proxy("data2")); // leak
	use2(get_proxy2()); // core dump
}

namespace Cpp11
{
	void use(Proxy* ptr)
	{
		if (ptr)
			ptr->request();
	}

	void use(std::unique_ptr<Proxy> ptr)
	{
		if (ptr)
			ptr->request();
	} // delete for ptr
}

using Matrix = vector<vector<int>>;

Matrix create_large_matrix()
{
	Matrix m{100'000, vector<int>(100'000)};

	return m;
}

void pointers_cpp11()
{
	using namespace Cpp11;

	unique_ptr<Proxy> up1(new Proxy("d3"));

	use(up1.get());

	use(move(up1));

	assert(up1.get() == nullptr);

	Matrix outer_m = create_large_matrix();
}